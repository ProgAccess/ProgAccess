<?php $tr=array (
  '_' => 'sendconfirm',
  '_todo_level' => 0,
  '_last_author' => 'Corentin',
  '_last_modif' => 1557327912,
  'title' => 'Confirmation de votre compte membre',
  'logo' => 'Logo',
  'subject' => 'Confirmation de compte {{site}}',
  'hello' => 'Bonjour {{name}},',
  'confirm' => 'Confirmez votre compte {{site}} en cliquant sur ce lien (expire après 24h)&nbsp;:',
  'click' => 'Cliquez ici',
  'auto' => 'Ce mail a été envoyé automatiquement, merci de ne pas répondre. Si vous n\'avez pas créé de compte {{site}}, vous pouvez ignorer ce message (mais tout de même jeter un œil à notre site ;)).',
  'signature' => 'Cordialement,<br>L\'administration {{site}}',
  'plaintext' => 'Bonjour {{name}}

Confirmez votre compte {{site}} en suivant cette adresse (expire après 24h):
{{link}}

Ce mail a été envoyé automatiquement, merci de ne pas répondre.

Cordialement,
L\'administration {{site}}',
  'admin' => 'L\'administration {{site}}',
  'team' => 'L\'administration {{site}}',
); ?>
