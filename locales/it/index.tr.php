<?php $tr=array (
  '_' => 'index',
  '_todo_level' => 4,
  '_last_author' => 'Pascal',
  '_last_modif' => 1621417465,
  'maintext' => '<p>Per evitare che internet sia una risorsa esclusiva e privata, i luttoni lo rendono più aperto e accessibile.</p>

<h3>Accessibile</h3>
<p>Una cosa accessibile (Quelqu\'elle) è utilizzabile dappertutto, da tutti, tutto il tempo (se possibile). Internet (o informatica in generale) è un bene comune, lavoro intellettuale e materiale dell\'umanità, che deve essere accessibile a tutti. Ciò implica che deve essere anche utilizzabile con o senza handicap, qualunque sia il budget, la geografia, la dimensione dello schermo, le prestazioni del computer, ecc.</p>
<p>L\'accessibilité d\'un logiciel ou d\'un matériel est déterminée par le prix, mais surtout par des choix techniques à tous les niveaux. À nous de faire les bons choix, de développer des outils appropriés (logiciels, études, normes), et de l\'imposer aux éditeurs et aux fabricants.</p>

<h3>Aperto</h3>
<p>Quando diciamo che qualcosa è aperto, significa che questa cosa non è una scatola nera di cui l\'utente potrebbe premere soltanto i tasti esterni, precedentemente decisi da qualcun\'altro di cui le intenzioni sono sconosciute. Queste scatole nere sono principalmente il lavoro dei giganti del Web, la GAFAM e società (Google, Apple, Facebook, Amazon, Microsoft), che parallelamente sviluppare le loro tecnologie nella massima segretezza. Ma abbiamo una soluzione: Open-Source è una garanzia per l\'utente di sapere esattamente che cosa fa un programma sul suo computer. Questa pratica è quella di pubblicare il codice sorgente (ricetta) di un programma per computer. Ognuno è libero di studiare come funziona, di modificarlo e di migliorarlo. Queste libertà, da utilizzare, studiare, condividere e modificare, sono garantite da licenze gratuite. (<a href="https://gnu.org">maggiori informazioni qui</a>)</p>
<p>Vuoi delle buone notizie? Beh, aiutando a rendere aperta la rete, può aiutarci a renderlo accessibile, perché se il codice di un progetto è pubblico e libero, allora chiunque può migliorarlo, anche a livello di accessibilità.</p>

<h3>Gli obiettivi delle proteste</h3>
<p>Nel lancio iniziale del 2015, questo sito ha lottato per l\'accessibilità con questo solo mezzo: identificare i windows utilizzabili per visual Deficients. Oggi, la comunità attiva del sito è cambiata, la squadra è cresciuta e siamo consapevoli che questo significa solo non basta. Abbiamo quindi mantenuto l\'attività iniziale, ma abbiamo esteso la nostra portata d azione</p>
<ul>
<li>Identificare intuitamente e organizzare software utilizzabile da tutti sotto le finestre, Android o GNU/Linux</li>
<li>Proporre tutoriali (audio o scritto) e un servizio di assistenza per computer remoto (che sta per arrivare) per permettere agli utenti meno sviluppati di gestire il meglio possibile con le attrezzature</li>
<li>Promuovere lo scambio e la partecipazione (il forum, tuttora in fase di sviluppo, dovrebbe essere in qualche modo)</li>
<li>Creare e difendere un software libero (o una cultura libera in generale)</li>
<li>Per cambiare atteggiamento e sensibilizzare l\'estrema importanza dell accessibilità in tutti i settori (tecnologia dell\'informazione, sanità, commercio, tempo libero e altro)</li>
<li>Propongono prove di hardware più o meno specializzate.</li>
</ul>
<p>Per raggiungere questo obiettivo, una squadra che comprende principalmente i giovani lavora per codificare e mantenere il sito, puoi scoprirlo qui.
Confidiamo anche su di voi senza il quale il sito non esisterebbe oggi.</p>
<p>Un messaggio da portare all\'amministrazione del sito? <a href="/contact_form.php">Contattaci</a>.</p>

<h2>Comment aider&nbsp;?</h2>
<p>Vous êtes convaincu, ce site vous plaît et vous voulez l\'aider&#8239;? Suivez le guide&nbsp;:</p>
<ul>
<li>Proposez votre aide pour coder le site, le traduire, mettre à jour des logiciels, proposer des tutoriels et autres via le <a href="/contact_form.php">formulaire de contact</a>.</li>
<li>Faites connaître le site pour l\'aider à se développer, notamment via le <a href="/reco.php">formulaire de recommandation</a> ou en le partageant sur les réseaux sociaux.</li>
<li>Téléchargez <a href="/opensource.php">le code source du site</a> {{lastosv}} et améliorez-le&#8239;! Il est disponible sous licence GNU AGPL v3.</li>
</ul>

<h2>On parle de nous&#8239;!</h2>
<p>Ils nous ont fait l\'honneur de publier des articles sur {{nomdusite}}&nbsp;:</p>
<ul>
<li><a href="https://sospc.name/sospc-aime-ProgAccess33-net/">"SOSPC aime&nbsp;: ProgAccess33.net"</a></li>
<li><a href="https://korben.info/ressources-non-mal-voyants.html">Korben&nbsp;: "Des ressources pour les non / mal voyants"</a></li>
<li><a href="http://forums.cnetfrance.fr/topic/1367199-ProgAccess33--un-site-dedie-aux-non-voyants-qui-facilite-l-acces-aux-logiciels/">CNet&nbsp;: "ProgAccess33&nbsp;: un site dédié aux non-voyants qui facilite l\'accès aux logiciels"</a></li>
</ul>

<h2>Site aux normes</h2>
<p>Parce que l\'accessibilité passe surtout par des aspects techniques, le respect de ces normes en est une garantie&nbsp;:</p>
<ul>
<li>CSS 3.0</li>
<li>WCAG 2.0 (Level AA)</li>
<li>HTML 5.1</li>
</ul>',
); ?>
