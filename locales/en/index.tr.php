<?php $tr=array (
  '_' => 'index',
  '_todo_level' => 4,
  '_last_author' => 'Pascal',
  '_last_modif' => 1621417452,
  'title' => '{{slogan}}',
  'happynewyear' => 'The hole team {{site}} wishes you an happy new year {{year}}!',
  'texttitle' => '{{slogan}}!',
  'maintext' => '<p>In order to prevent Internet from being an exclusive and private resource, let\'s make it more open and accessible.</p>

<h3>Accessible</h3>
<p>Accessible (whatever it is) means that this is usable everywhere, by everybody, and always (as far as possible).
Internet (or generally computing) is a common good, which has to be accessible for everybody. This implies that it must be usable with or without disabilities, regardless of budget, location, screen size, computer performance, etc.</p>
<p>Accessibility of a software or a hardware is determined by price, but especially by technical choices at all levels. We must make the right choices, develop suitable tools (softwares, studies, norms), and force editors and manufacturers to do the same.</p>

<h3>Open</h3>
<p>When we say something that is open, it means that this thing is not a poor deposit box which the user could only press the external buttons, decided by someone else we do not know the intentions. These boxes are mainly the artwork of web giants, GAFAM and company (Google, Apple, Facebook, Amazon, Microsoft), who keep developing their technologies in parallel in the greatest secrecy.<br>
However, we have a great solution: Open-Source is a guarantee for the user to know exactly what a program can do on their computer. This practice consists of publishing the source code (the recipe) of a computer program. Everyone is then free to study how it works, to modify it and to improve it. These freedoms &#8211; to use, study, share, and modify &#8211; are guaranteed by free (libre) licenses. (<a href="https://gnu.org">More info here</a>)</p>
<p>You want a good news? Well, by contributing to make the open web, we can help making it accessible, because if the code of a project is public and free, anyone can improve it, even with the accessibility level.</p>

<h3>{{site}}\' goals</h3>
<p>At its initial launch in 2015, this site struggled for accessibility with this only way: listing softwares under Windows that are used by visually impaired people.<br>
At the time we\'re in, the active community of the site has changed, the team has grown, and we are aware that this way is not enough to do the whole job.<br>
So we kept the initial work, but expanded our field of action:</p>
<ul>
	<li>List in an intuitive and organized way the software that can be used by everybody under Windows, Android or GNU / Linux;</li>
	<li>Propose some audio or written tutorials and, in the futur, a remote computer assistance service allowing beginner users to get better with their device;</li>
	<li>Promote exchange and participation with the <a href="https://forum.progaccess.net">forum</a>;</li>
	<li>Create and defend a maximum free software (or free culture in general &#8211; free as in freedom);</li>
	<li>Increase the attitudes and raise awareness of the extreme importance of accessibility in all areas (computing, health, trade, leisure and more);</li>
	<li>Propose tests of more or less specialized equipment.</li>
</ul>
<p>To achieve this, a team of mostly young people work to code and maintain the site, you can <a href="/contact.php">see it here</a>.<br>
We also rely on all of you, without whom the site wouldn\'t exist today.</p>
<p>Did you find a bug or do you want to message the administration of the site? <a href="/contact_form.php">Don\'t hesitate to contact us.</a></p>

<h2>How to help?</h2>
<p>Do you like this site and want to help? Follow the guide:</p>
<ul>
	<li>Submit your help to code the site, update software, offer tutorials and more by using the <a href="/contact_form.php">contact form</a>.</li>
	<li>Make the site known to keep developing, by sharing it on social networks.</li>
	<li>If you have web or PHP programming skills, see <a href="/opensource.php">the source code of the site</a> and improve it! It is available under the GNU AGPL v3 license.</li>
</ul>

<h2>They\'re talking about us!</h2>
<p>They publish articles about {{site}}:</p>
<ul>
<li><a href="https://sospc.name/sospc-aime-ProgAccess33-net/">"SOSPC aime&nbsp;: ProgAccess33.net" (fr)</a></li>
<li><a href="https://korben.info/ressources-non-mal-voyants.html">Korben&nbsp;: "Des ressources pour les non / mal voyants" (fr)</a> resources for visually impaired people</li>
<li><a href="http://forums.cnetfrance.fr/topic/1367199-ProgAccess33--un-site-dedie-aux-non-voyants-qui-facilite-l-acces-aux-logiciels/">CNet&nbsp;: "ProgAccess33&nbsp;: un site dédié aux non-voyants qui facilite l\'accès aux logiciels" (fr)</a></li>
</ul>

<h2>Standards</h2>
<p>Because computers accessibility is almost made of technical purposes, respecting the norms is needed:</p>
<ul>
<li>CSS 3.0</li>
<li>WCAG 2.0 (Level AA)</li>
<li>HTML 5.1</li>
</ul>',
'mailconfirmtext' => 'Thanks for your message. You should receive an answer e-mail soon.',
); ?>
