<?php $tr=array (
  '_' => 'search',
  '_todo_level' => 1,
  '_last_author' => 'Pascal',
  '_last_modif' => 1562494449,
  'title' => 'Serĉo',
  'title2' => 'Serĉi {{terms}}',
  'noresult' => 'Pardonu, ni nenion trovas pri "{{terms}}".',
  'found' => '{{count}} rezultoj trovitaj en {{time}} ms.',
  'result_hits' => '{{hits}} vizitoj, {{dl}} elŝutitoj',
); ?>
